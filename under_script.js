const names = ["Petya", "Vasya", "Kolya", "Olya", "Volodymyr", "Elizabeth", "Izabella"];

const rightNames = function(value) {
    if (value.length > 4 && value.length < 9) {
        return value;
    }
}

console.log(_.find(names, rightNames));